<h1 align="center">Client App - Realtime Machine Learning</h1>
<h4 align="center"><strong>Aron Hemmes</strong></h4>
<p align="center">An implementation of object recognition and AI in the environment of a game called Valorant.</p>
<br><br>

## Getting Started

### Setup

To start the app in development mode run:
```commandline
npm i
npm start
```

To build the app to release run:
```commandline
npm run release
```